/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package iotappfx;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.io.gpio.GpioPinDigitalOutput;
import com.pi4j.io.gpio.PinState;
import com.pi4j.io.gpio.RaspiPin;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

/**
 *
 * @author bluesprogrammer
 */
public class FXMLDocumentController implements Initializable {   
    
        final GpioController gpio = GpioFactory.getInstance();
        final GpioPinDigitalOutput pin =
        gpio.provisionDigitalOutputPin(RaspiPin.GPIO_07, "Relay1", PinState.LOW);  
        final GpioPinDigitalOutput pin2 =
        gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, "Relay2", PinState.LOW);
        final GpioPinDigitalOutput pin3 =
        gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "Relay3", PinState.LOW);
        final GpioPinDigitalOutput pin4 =
        gpio.provisionDigitalOutputPin(RaspiPin.GPIO_02, "Relay4", PinState.LOW);
    
    
    @FXML
    private void offRelay1Button(ActionEvent event) {
        pin.high();
        System.out.println("Relay off");       
    }
    
    @FXML
    private void onRelay1Button(ActionEvent event){      
        pin.low();
        System.out.println("Relay on"); 
    }
    
    @FXML
    private void offRelay2Button(ActionEvent event) {
        pin2.high();
        System.out.println("Relay off");       
    }
    
    @FXML
    private void onRelay2Button(ActionEvent event){      
        pin2.low();
        System.out.println("Relay on"); 
    }
    
    @FXML
    private void offRelay3Button(ActionEvent event) {
        pin3.high();
        System.out.println("Relay off");       
    }
    
    @FXML
    private void onRelay3Button(ActionEvent event){      
        pin3.low();
        System.out.println("Relay on"); 
    }
    
    @FXML
    private void offRelay4Button(ActionEvent event) {
        pin4.high();
        System.out.println("Relay off");       
    }
    
    @FXML
    private void onRelay4Button(ActionEvent event){      
        pin4.low();
        System.out.println("Relay on"); 
    }
  
    @FXML
    private void destroyButton(ActionEvent event){
        Platform.exit();
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
    }    
    
}

